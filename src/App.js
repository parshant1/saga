import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";
import AddEditUser from "./pages/AddEditUser";
import Users from "./pages/Users";
import ViewUser from "./pages/ViewUser";

function App() {
	return (
		<>
			<Router>
				<Switch>
					<Route exact path="/">
						<Users />
					</Route>
					<Route exact path="/addUser">
						<AddEditUser />
					</Route>
					<Route exact path="/editUser/:id">
						<AddEditUser />
					</Route>
					<Route path="/viewUser/:id">
						<ViewUser />
					</Route>
				</Switch>
			</Router>
		</>
	);
}

export default App;
