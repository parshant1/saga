import React, { useEffect } from "react";
import MaterialTable from "material-table";
import { useDispatch, useSelector } from "react-redux";
import { loadUserStart } from "../redux/actions/action";

const Users = () => {
  //   const { users } = useSelector((state) => state.userReducer.users);
  //   console.log("USERS DATA ", users);

  const dispatch = useDispatch();
  useEffect(() => {
    const fetchUser = () => {
      return dispatch(loadUserStart);
    };

    fetchUser();
  }, []);
  return (
    <div>
      <MaterialTable
        columns={[
          { title: "SN", field: "id" },
          { title: "Name", field: "name" },
          { title: "Email", field: "email" },
          {
            title: "Country",
            field: "country",
          },
        ]}
        title="JHGJHGJHGJHG"
      />
    </div>
  );
};

export default Users;
