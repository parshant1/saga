import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createUserStart } from "../redux/actions/action";

const initialState = {
  name: "",
  email: "",
  country: "",
};

const AddEditUser = () => {
  const [formValue, setFormValue] = useState(initialState);
  const { name, email, country } = formValue;

  const dispatch = useDispatch();

  const changeHandler = (e) => {
    const updates = {
      ...formValue,
      [e.target.name]: e.target.value,
    };
    setFormValue(updates);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (name && email && country) {
      dispatch(createUserStart(formValue));
      console.log("submitted")
    }
  };

  return (
    <div>
      <form action="">
        <input
          type="text"
          placeholder="Name"
          name="name"
          onChange={changeHandler}
        />{" "}
        <br />
        <input
          type="text"
          placeholder="Email"
          name="email"
          onChange={changeHandler}
        />{" "}
        <br />
        <input
          type="text"
          placeholder="Country"
          name="country"
          onChange={changeHandler}
        />{" "}
        <br />
        <button onClick={submitHandler}>Add</button>
      </form>
    </div>
  );
};

export default AddEditUser;
