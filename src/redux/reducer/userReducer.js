import * as types from "../types";

const initialState = {
  users: [],
  loading: false,
  errors: " ",
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOAD_USERS_START:
      return {
        ...state,
        loading: true,
      };
    case types.LOAD_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        users: action.payload,
      };
    case types.LOAD_USERS_ERROR:
      return {
        ...state,
        loading: false,
        errors: action.payload,
      };
    case types.CREATE_USER_START:
      return {
        ...state,
        loading: true,
        users: action.payload,
      };
    case types.CREATE_USER_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case types.CREATE_USER_ERROR:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export default userReducer;
