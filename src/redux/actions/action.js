import * as types from "../types";

export const loadUser = () => ({
  type: types.LOAD_USERS,
});

export const loadUserStart = () => ({
  type: types.LOAD_USERS_START,
});

export const loadUserSuccess = (users) => ({
  type: types.LOAD_USERS_SUCCESS,
  payload: users,
});

export const loadUserError = (errors) => ({
  type: types.LOAD_USERS_ERROR,
  payload: errors,
});

export const createUserStart = (user) => ({
  type: types.CREATE_USER_START,
  payload: user,
});

export const createUserSuccess = () => ({
  type: types.CREATE_USER_SUCCESS,
});

export const createUserError = () => ({
  type: types.CREATE_USER_ERROR,
});
