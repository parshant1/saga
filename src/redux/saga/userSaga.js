import * as types from "../types";

import {
  takeEvery,
  take,
  takeLatest,
  put,
  all,
  delay,
  fork,
  call,
} from "redux-saga/effects";

import httpClient from "http-client";

import {
  loadUserSuccess,
  loadUserError,
  createUserSuccess,
  createUserError,
  loadUser
} from "../actions/action";

import { loadUserApi, createUserApi } from "../../api";

export function* loadUserSync() {
  const json = yield fetch("https://reqres.in/api/users?page=2")
        .then(response => response.json(), );    
  yield put(loadUserSuccess(json));
  // console.log("user data", data);

  // if (error) {
  //   yield put(loadUserError(error));
  // } else {
  //   yield put(loadUserSuccess(data));
  // }
}

export function* createUserSaga({payload}) {
  const { error, data } = yield call(createUserApi, payload);
  console.log("user data", data);

  if (error) {
    yield put(createUserError());
  } else {
    yield put(createUserSuccess());
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest(types.LOAD_USERS_START, loadUserSync),
    takeLatest(types.CREATE_USER_START, createUserSaga),
  ]);
}
