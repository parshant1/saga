import axios from "axios";

export async function axiosRequest(url, method, headers, params) {
  return params
    ? axios({
        method: method,
        url: url,
        data: params,
        headers: headers,
        timeout: 1000,
      })
    : axios({
        method: method,
        url: url,
        data: {},
        headers: headers,
        timeout: 1000,
      });
}

const loadUserApi = () => {
  const headers = {
    "Content-Type": "application/json",
  };
  return axiosRequest("http://localhost:3400/users", "GET", headers, {});
};

const createUserApi = (user) => {
  const headers = {
    "Content-Type": "application/json",
  };
  return axiosRequest("http://localhost:3400/users", "POST", headers, user);
};

export { loadUserApi, createUserApi };
